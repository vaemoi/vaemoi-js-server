const
  dotenv = require(`dotenv`),
  express = require(`express`),
  expressStatic = require(`express-static-gzip`);

dotenv.config();

const
  app = express(),
  appPort = process.env.port,
  appEnv = process.env.NODE_ENV;

/* Middleware goes here */
app.use(`/`, expressStatic(`${__dirname}/public`));

/* Routes */
app.get(`/hello`, (request, response) => {
  response.send(`hello`)
});

app.listen(appPort, () => {
  if (appEnv === `development`) {
    const bsConfig = {
      files: [
        `${__dirname}/public/css/main.css`
      ],
      open: false,
      plugins: [{
        module: `bs-html-injector`,
        options: {
          files: [`public/*.html`]
        }
      }],
      server: `public`
    };

    require(`browser-sync`).init(bsConfig);
  }
});
